# This file is part of grateful_dead_lyrics.

# grateful_dead_lyrics is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# grateful_dead_lyrics is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with grateful_dead_lyrics.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License


FORTUNES  = grateful-dead{,.dat}

DESTDIR   =
PREFIX    = /usr
INSTDIR   = $(DESTDIR)$(PREFIX)/share/fortune


.PHONY: all clean install uninstall


all:
	$(SHELL) ./src/fortunize.sh


clean:
	$(RM) grateful-dead{,.dat}


install:
	test -d $(INSTDIR) || mkdir -p $(INSTDIR)

	install -m 0644 $(FORTUNES) $(INSTDIR)


uninstall:
	$(RM) $(INSTDIR)/$(FORTUNES)
