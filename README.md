# Grateful Dead lyrics

## Origin

Lyrics taken from from https://www.cs.cmu.edu/~mleone/dead-lyrics.html.

Using this:
```bash
wget --recursive -np -nc -nH --cut-dirs=4 --random-wait --wait 1 "https://www.cs.cmu.edu/~mleone/dead-lyrics.html"
```


# License

The scripts are licensed under GPLv3 license.

The lyrics of course belong to their respective owners.
