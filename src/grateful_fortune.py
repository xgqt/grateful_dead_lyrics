#!/usr/bin/env python3


"""
This file is part of grateful_dead_lyrics.

grateful_dead_lyrics is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

grateful_dead_lyrics is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with grateful_dead_lyrics.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License

grateful-fortune
"""


import os
import random


def gather_quotes():
    """Gather quotes from lyrics files"""
    quotes = []

    for _, _, files in os.walk("./lyrics"):
        for lyrics_file in files:
            with open("./lyrics/" + lyrics_file) as l_f:
                quote = []
                for line in l_f.read().splitlines():
                    if line != "":
                        quote.append(line)
                    else:
                        quotes.append(quote)
                        quote = []
    return quotes


def print_random_quote(quotes):
    """Print gathered quotes"""
    quote = random.choice(quotes)

    print("''''")

    for line in quote:
        print("  " + line)

    print("''''")


def main():
    """Main"""
    print_random_quote(gather_quotes())


if __name__ == '__main__':
    main()
